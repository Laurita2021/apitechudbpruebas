package com.techu.apitechudbpruebas.controllers;

import com.techu.apitechudbpruebas.ProductModel.ProductModel;
import com.techu.apitechudbpruebas.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/apitechu/pruebas")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping ("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("en getProducts");
        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }
}
