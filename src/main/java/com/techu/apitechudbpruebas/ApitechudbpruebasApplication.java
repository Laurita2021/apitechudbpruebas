package com.techu.apitechudbpruebas;

import com.techu.apitechudbpruebas.ProductModel.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbpruebasApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbpruebasApplication.class, args);

		ApitechudbpruebasApplication.productModels = ApitechudbpruebasApplication.getTestData();
	}

	private static ArrayList<ProductModel> getTestData() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,20
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,30
				)
		);

		return productModels;
	}

}
