package com.techu.apitechudbpruebas.repositories;

import com.techu.apitechudbpruebas.ApitechudbpruebasApplication;
import com.techu.apitechudbpruebas.ProductModel.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductRepository");
        return ApitechudbpruebasApplication.productModels;
    }
}
