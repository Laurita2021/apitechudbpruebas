package com.techu.apitechudbpruebas.services;

import com.techu.apitechudbpruebas.ProductModel.ProductModel;
import com.techu.apitechudbpruebas.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;
    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductService");
        return this.productRepository.findAll();
    }
}
